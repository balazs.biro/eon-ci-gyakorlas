public with sharing class DemoController {
   
         /**
     * An empty constructor for the testing
     */
    public DemoController() {

                if (b == 0) {  // Noncompliant
        doOneMoreThing();
        } else {
        doOneMoreThing();
        }

        switch on i {  // Noncompliant
        when 1 {
            doSomething();
        }
        when 2 {
            doSomething();
        }
        when 3 {
            doSomething();
        }
        when else {
            doSomething();
        }
        }
    }

    private valami(){}
    /**
     * Get the version of the SFDX demo app
     */
    public String getAppVersion() {
        return '1.0.0';
    }


    public static void myFunction(Task[] tasks) {
        Integer i = 0;
        while ( i < 1000) {
            Task task = tasks[i];
            if (task.subject == 'foo') {
                task.subject = 'bar';
                update task; // Noncompliant
            }
        }
    }

    Integer foo(Integer a) {
        Integer i = 10;
        return i + a; // Noncompliant
        i++; // dead code
    }

    

}
